int pot_pin[] = {A0, A2, A4, A6};
int out_pin[] = {11, 10, 9, 3};
int button_pin[] = {A1, A3, A5, A7};

long brightness[4];


void setup() {
  Serial.begin(9600);

  for (int i = 0; i < 4; i++) {
    pinMode(pot_pin[i], INPUT);
    pinMode(button_pin[i], INPUT);
    pinMode(out_pin[i], OUTPUT);
    //brightness[i] = 255 - (long) analogRead(pot_pin[i]) * 255 / 1024;
  }

}


void loop() {
  Serial.println("Pot positions:");
  for (int i = 0; i < 4; i++) {
    Serial.println(analogRead(pot_pin[i]));
  }
  Serial.println();

  Serial.println("Buttons:");
  for (int i = 0; i < 4; i++) {
    Serial.println(analogRead(button_pin[i]));
  }
  Serial.println();

  Serial.println("Brightness:");
  for (int i = 0; i < 4; i++) {
    int reading = 0;
    for (int j = 0; j < 10; j++) {
      reading += analogRead(pot_pin[i]);
      delay(1);
    }
    reading /= 10;
    brightness[i] = (brightness[i] + (long) reading * 255 / 1024) /2;
    Serial.println(brightness[i]);
  }
  Serial.println();
  
  for (int i = 0; i < 4; i++) {    
    analogWrite(out_pin[i], 255 - brightness[i]);
  }

  //delay(30);
}
